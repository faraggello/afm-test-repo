jQuery(document).ready(function(){

	// for test with true data
	// email : demo4@takatuf.tk
	// pass : demopass4

	// define my api main path //////////// 
  var api_url="http://www.takatuf.tk/api/public/";

  // define my variables texts
  var alreadyRegisteredMessage="You already logged in as ";
  var congratulationRegisteredMessage="Congratulation , your data registered successfully"

	///// Login request method ///////////////
  function LoginRequest (api_url,api_parameters){
    
    $.ajax({
      "async": true,
      "crossDomain": true,
      "url": api_url+api_parameters,
      "method": "POST",
      "headers": {},
      "processData": false,
      "contentType": false,
      "mimeType": "multipart/form-data"
    })

    .complete(function (response) {
      // parse my json result
      response=JSON.parse(response.responseText);

      // check my result
      if (response.code ==302){
        // store the user ID
        localStorage.setItem("userID",response.data.userID);
        // store the user name
        localStorage.setItem("userName",response.data.userName);
        // store the user token
        localStorage.setItem("userToken",response.data.userToken);
        // redirect to home
        document.location.href = 'home.html';
      }else{
        // show notification message popup
        showPopup("notification-popup-login",response.data.message);
        // empty the store user ID
        localStorage.removeItem("userID");
        // empty the store user name
        localStorage.removeItem("userName");
        // empty the store user token
        localStorage.removeItem("userToken");      
      }
    });

  }; // LoginRequest


  ///// Register request method ///////////////
  function RegisterRequest (api_url,api_parameters){

    $.ajax({
      "async": true,
      "crossDomain": true,
      "url": api_url+api_parameters,
      "method": "POST",
      "headers": {},
      "processData": false,
      "contentType": false,
      "mimeType": "multipart/form-data"
    })

    .complete(function (response) {
      // parse my json result
      response=JSON.parse(response.responseText);
      // check my result
      if (response.code ==302){
        // show congratulation popup
        showPopup("notification-popup-register",congratulationRegisteredMessage);
      }else{
        // show notification message popup
        showPopup("notification-popup-register",response.data.message);     
      }
    });

  }; // RegisterRequest


  // show notification popup method
  function showPopup(div_id,message_text){
    $("#"+div_id+" h4").empty();
    $("#"+div_id+" h4").append(message_text);
    $("#"+div_id).popup("open");
  }; //showPopup

	// login api reuqest/////////////////////////
	$("#form-login .user-login").click(function() {

    // check if already user or not 
    if (localStorage.getItem("userID") && localStorage.getItem("userName") && localStorage.getItem("userToken")){
      
      // show the popup message       
      showPopup("notification-popup-login",alreadyRegisteredMessage+" "+localStorage.getItem("userName"));
    
    }else{

      // check user email
      str =$("#form-login #email").val()
      if (str.indexOf('@') == -1 || str.indexOf('.') == -1){
        $("#form-login #email").addClass("alert");
        $("#form-login #email").focus();
        return false
      }else{
        $("#form-login #email").removeClass("alert");
      }

      // check user password      
      if($("#form-login #password").val()==""){
        $("#form-login #password").addClass("alert");
        $("#form-login #password").focus();
        return false
      }else{
        $("#form-login #password").removeClass("alert");
      }

      submit=true;
      
      if(submit){
        var api_parameters="accounts/login.json?password="+$("#form-login #password").val()+"&email="+$("#form-login #email").val();
        // calling my login request method
        LoginRequest(api_url,api_parameters);
      } // submit

    } // check if logged

	}); // user-login

	// login reset form /////////////////////////
	$("#form-login .reset").click(function() {
		$("#form-login input").val("");
	}); // user-login

  // sign out /////////////////////////////////
  $(".exit-btn").click(function() {
    // empty the store user ID
    localStorage.removeItem("userID");
    // empty the store user name
    localStorage.removeItem("userName");
    // empty the store user token
    localStorage.removeItem("userToken");
    // redirect to login page
    document.location.href = 'login.html';
  }); // exit-btn


  // register api reuqest/////////////////////////
  $("#form-register .user-register").click(function() {

    // check if already user or not 
    if (localStorage.getItem("userID") && localStorage.getItem("userName") && localStorage.getItem("userToken")){
      
      // show the popup message       
      showPopup("notification-popup-register",alreadyRegisteredMessage+" "+localStorage.getItem("userName"));
    
    }else{

      // check username      
      if($("#form-register #username").val()==""){
        $("#form-register #username").addClass("alert");
        $("#form-register #username").focus();
        return false
      }else{
        $("#form-register #username").removeClass("alert");
      }

      // check user fullname      
      if($("#form-register #fullname").val()==""){
        $("#form-register #fullname").addClass("alert");
        $("#form-register #fullname").focus();
        return false
      }else{
        $("#form-register #fullname").removeClass("alert");
      }

      // check user email
      str =$("#form-register #email").val()
      if (str.indexOf('@') == -1 || str.indexOf('.') == -1){
        $("#form-register #email").addClass("alert");
        $("#form-register #email").focus();
        return false
      }else{
        $("#form-register #email").removeClass("alert");
      }

      // check user password      
      if($("#form-register #password").val()==""){
        $("#form-register #password").addClass("alert");
        $("#form-register #password").focus();
        return false
      }else{
        $("#form-register #password").removeClass("alert");
      }

      // confirm password
      if($("#form-register #re_password").val() !== $("#form-register #password").val()){
        $("#form-register #re_password").addClass("alert");
        $("#form-register #re_password").focus();
        return false
      }else{
        $("#form-register #re_password").removeClass("alert");
      }

      register=true;
      
      if(register){
        var api_parameters="accounts/create.json?username="+$("#form-register #username").val()+"&fullname="+$("#form-register #fullname").val()+"&password="+$("#form-register #password").val()+"&email="+$("#form-register #email").val();
        // calling my register request method
        RegisterRequest(api_url,api_parameters);
      } // register

    } // check if logged

  }); // user-login

  // just for test user data //////////////////////////
  console.log(localStorage.getItem("userID"));
  console.log(localStorage.getItem("userName"));
  console.log(localStorage.getItem("userToken"));

});